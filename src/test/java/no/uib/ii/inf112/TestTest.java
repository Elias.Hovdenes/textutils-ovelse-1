package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			if (width <= 0 || text.length() > width) {
				return null;
			}
			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			if (width <= 0) {
				return null;
			}
			
			int textLength = text.length();
			int extra = width - textLength;
			return " ".repeat(extra) + text;

		}

		public String flushLeft(String text, int width) {
			int textLength = text.length();
			int extra = width - textLength;
			return text + " ".repeat(extra);
		}

		@Override
		public String justify(String text, int width) {
			// TODO Auto-generated method stub
			return null;
		}

		//Make a method that takes a string and a width and returns a list of strings that are the lines of the justified text.
		// public String justify(String text, int width) {
		// 	ArrayList<String> lines = new ArrayList<String>();
		// 	String[] words = text.split(" ");
		// 	String line = "";
		// 	for (String word : words) {
		// 		if (line.length() + word.length() + 1 <= width) {
		// 			line += word + " ";
		// 		} else {
		// 			lines.add(line);
		// 			line = word + " ";
		// 		}
		// 	}
		// 	lines.add(line);
		// 	return lines;
		// }

	
	};

		

	@Test
	void test() {
		//fail("Not yet implemented");
	}

	@Test
	void testCenter() {
		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals(" fo ", aligner.center("fo", 5));
		assertEquals(null, aligner.center("Hello", -1));
		assertEquals(null, aligner.center("Hello", 3));
		assertEquals("Hello", aligner.center("Hello" , 5));
		assertEquals(" Hello ", aligner.center("Hello", 8));

	}

	@Test
	void testFlushRight() {
		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals(null, aligner.flushRight("Hello", -1));
		assertEquals("    Gam", aligner.flushRight("Gam", 7));
	}

	@Test
	void testFlushLeft() {
		assertEquals("A    ", aligner.flushLeft("A", 5));

	}

	@Test
	void testJustify() {
		assertEquals("Hei   Hei", aligner.justify("Hei hei", 9));
	}
}
